//
//  ScoreTests.swift
//  20190802-MichaelVilabrera-NYCSchoolsTests
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import XCTest
@testable import _0190802_MichaelVilabrera_NYCSchools

class ScoreTests: XCTestCase {
    
    var score: Score!
    
    override func tearDown() {
        score = nil
    }
    
    func test_Init_TakesDBN() {
        score = Score(dbn: "stuff")
        XCTAssertEqual(score.dbn, "stuff")
    }
    
    func test_Init_TakesDBNAndSchoolName() {
        score = Score(dbn: "", schoolName: "Riverdale High")
        XCTAssertEqual(score.schoolName, "Riverdale High")
    }
    
    func test_Init_TakesDBNAndSchoolNameAndAttendance() {
        score = Score(dbn: "", schoolName: "", attendance: "123")
        XCTAssertEqual(score.attendance, "123")
    }
    
    func test_Init_TakesDBNAndSchoolNameAndAttendanceAndReading() {
        score = Score(dbn: "", schoolName: "", attendance: "0", reading: "951")
        XCTAssertEqual(score.reading, "951")
    }
    
    func test_Init_TakesDBNAndSchoolNameAndAttendanceAndReadingAndWriting() {
        score = Score(dbn: "", schoolName: "", attendance: "0", reading: "0", writing: "642")
        XCTAssertEqual(score.writing, "642")
    }
    
    func test_Init_TakesDBNAndSchoolNameAndAttendanceAndReadingAndWritingAndMath() {
        score = Score(dbn: "", schoolName: "", attendance: "0", reading: "0", writing: "0", math: "531")
        XCTAssertEqual(score.math, "531")
    }
}
