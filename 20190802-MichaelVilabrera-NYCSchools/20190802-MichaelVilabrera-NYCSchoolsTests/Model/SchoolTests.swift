//
//  SchoolTests.swift
//  20190802-MichaelVilabrera-NYCSchoolsTests
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import XCTest
import CoreLocation
@testable import _0190802_MichaelVilabrera_NYCSchools

class SchoolTests: XCTestCase {
    
    var school: School!

    override func tearDown() {
        school = nil
    }
    
    func test_Init_TakesDBN() {
        school = School(dbn: "stuff")
        XCTAssertEqual(school.dbn, "stuff")
    }
    
    func test_Init_TakesDBNAndSchoolName() {
        school = School(dbn: "", name: "Riverdale High")
        XCTAssertEqual(school.name, "Riverdale High")
    }
    
    func test_Init_TakesDBNAndNameAndOverview() {
        school = School(dbn: "", name: "", overview: "view over")
        XCTAssertEqual(school.overview, "view over")
    }
    
    func test_Init_TakesLatLong() {
        school = School(dbn: "", name: "", overview: "", latitude: "0.1", longitude: "0.3")
        XCTAssertEqual(school.latitude, "0.1")
        XCTAssertEqual(school.longitude, "0.3")
    }
}
