## Synopsis

This project is my submission for the Mobile iOS Developer position with JPMC.

## Code Example

The sample provided takes data for high schools, SAT scores, joins them and shares information with the user. There is also a map which displays the names & locations of nearby schools.

## Installation

type 'open 20190802-MichaelVilabrera-NYCSchools.xcodeproj' in Terminal or double-click 20190802-MichaelVilabrera-NYCSchools.xcodeproj in Finder.

## Tests

With Xcode & the project open, press CMD-U to run unit tests.

## Tradeoffs

I did not prefer to use any logic when creating the SchoolViewEntity, however I felt it was necessary to adjust the display when a certain school did not offer SAT tests, or had no students take the SAT. Also, I wanted to add more tests especially around the logic in the presenter, school & score view entities.

## Wishlist

I wanted to write more unit tests, yet felt they would have impacted delivery.
Also hoping to refactor the networking and reduce some of the duplication for the school & score fetches.
I placed all strings into the Constants file, to enable localization where we could.
Adding sort by SAT performance, school attendance, or proximity to the user would be nice.
Finally I would prefer to convert annotations to allow the user to visually differentiate schools with and schools without SAT test takers.

### Thank you
