//
//  MapViewController.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: BaseViewController {
    
    private let mapView = MKMapView()
    
    private var presenter: MapPresenter?
    
    var locationManager = CLLocationManager()
    
    var viewable: MapViewable?
    
    var schoolViewEntities: [SchoolViewEntity]? {
        didSet {
            DispatchQueue.main.async {
                guard let sve = self.schoolViewEntities else { return }
                self.createAnnotations(sve)
            }
        }
    }
    
    init(presenter: MapPresenter) {
        
        super.init(nibName: nil, bundle: nil)
        
        self.presenter = presenter
        presenter.mapViewable = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewable = self
        
        locationManager.delegate = self
        mapView.delegate = self
        
        presenter?.viewDidLoad()
    }
    
    private func createAnnotations(_ schoolViewEntities: [SchoolViewEntity]) {
        let annotations = schoolViewEntities.compactMap { sve -> MKAnnotation in
            
            let annotation = MKPointAnnotation()
            annotation.title = sve.name
            annotation.coordinate = sve.coordinate
            
            return annotation
        }
        mapView.addAnnotations(annotations)
    }
    
    private func setupView() {
        
        mapView.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.topAnchor),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

extension MapViewController: CLLocationManagerDelegate, MapViewable {
    
    func startUpdatingLocation() {
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setCenter() {
        
        mapView.showsUserLocation = true
        
        let coord = locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 40.692351, longitude: -73.983482)
        
        let region = MKCoordinateRegion.init(center: coord, latitudinalMeters: 250.0, longitudinalMeters: 250.0)
        
        mapView.setRegion(region, animated: true)
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is SchoolViewEntity else { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Constants.Identifiers.annotation)
        
        if annotationView == nil {
            let pinView = MapAnnotationView(annotation: annotation, reuseIdentifier: Constants.Identifiers.annotation)
            pinView.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
            
            let hue = CGFloat(annotation.coordinate.longitude) / 100
            pinView.backgroundColor = UIColor(hue: hue, saturation: 0.5, brightness: 1, alpha: 1)
            
            annotationView = pinView
        }
        
        return annotationView
    }
}
