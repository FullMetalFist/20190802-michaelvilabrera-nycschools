//
//  MapViewable.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import CoreLocation

protocol MapViewable: BaseViewable {
    var locationManager: CLLocationManager { get set }
    var schoolViewEntities: [SchoolViewEntity]? { get set }
    
    func startUpdatingLocation()
    func setCenter()
}
