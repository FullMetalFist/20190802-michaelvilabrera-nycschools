//
//  DetailViewController.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: BaseViewController {
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let mapView = MKMapView()
    let titleLabel = UILabel()
    let overviewLabel = UILabel()
    let attendanceHeader = UILabel()
    let attendanceLabel = UILabel()
    let mathHeader = UILabel()
    let mathLabel = UILabel()
    let readingHeader = UILabel()
    let readingLabel = UILabel()
    let writingHeader = UILabel()
    let writingLabel = UILabel()
    
    var viewEntity: SchoolViewEntity
    
    init(_ viewEntity: SchoolViewEntity) {
        self.viewEntity = viewEntity
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        setupContentView()
        setupView()
        
        populateFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height + 10)
    }
    
    private func populateFields() {
        
        let region = MKCoordinateRegion.init(center: viewEntity.coordinate, latitudinalMeters: 250.0, longitudinalMeters: 250.0)
        
        mapView.setRegion(region, animated: true)
        mapView.addAnnotation(viewEntity)
        
        titleLabel.text = viewEntity.name
        overviewLabel.text = viewEntity.overview
        attendanceLabel.text = viewEntity.score.attendance
        mathLabel.text = viewEntity.score.math
        readingLabel.text = viewEntity.score.reading
        writingLabel.text = viewEntity.score.writing
        
        attendanceHeader.text = viewEntity.hasTests ? Constants.SATHeaders.attendance : Constants.SchoolError.noTest
        mathHeader.text = viewEntity.hasTests ? Constants.SATHeaders.math : ""
        readingHeader.text = viewEntity.hasTests ? Constants.SATHeaders.reading : ""
        writingHeader.text = viewEntity.hasTests ? Constants.SATHeaders.writing : ""
    }
    
    private func setupContentView() {
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.isScrollEnabled = true
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        let layoutMarginsGuide = view.safeAreaLayoutGuide
        
        let tabBarHeight = tabBarController?.tabBar.frame.size.height ?? 0
        
        scrollView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
        
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -tabBarHeight).isActive = true
    }
    
    private func setupView() {
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        overviewLabel.translatesAutoresizingMaskIntoConstraints = false
        attendanceHeader.translatesAutoresizingMaskIntoConstraints = false
        attendanceLabel.translatesAutoresizingMaskIntoConstraints = false
        mathHeader.translatesAutoresizingMaskIntoConstraints = false
        mathLabel.translatesAutoresizingMaskIntoConstraints = false
        readingHeader.translatesAutoresizingMaskIntoConstraints = false
        readingLabel.translatesAutoresizingMaskIntoConstraints = false
        writingHeader.translatesAutoresizingMaskIntoConstraints = false
        writingLabel.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.layer.cornerRadius = 8
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isScrollEnabled = false
        titleLabel.textAlignment = .center
        
        contentView.addSubview(mapView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(overviewLabel)
        contentView.addSubview(attendanceHeader)
        contentView.addSubview(attendanceLabel)
        contentView.addSubview(mathHeader)
        contentView.addSubview(mathLabel)
        contentView.addSubview(readingHeader)
        contentView.addSubview(readingLabel)
        contentView.addSubview(writingHeader)
        contentView.addSubview(writingLabel)
        
        mapView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        mapView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        mapView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        mapView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3, constant: 80.0).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: mapView.bottomAnchor, constant: 20).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        titleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.03, constant: 8.0).isActive = true
        titleLabel.numberOfLines = 2
        titleLabel.sizeToFit()
        
        overviewLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        overviewLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        overviewLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        overviewLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3, constant: 40.0).isActive = true
        overviewLabel.numberOfLines = 0
        overviewLabel.sizeToFit()
        
        attendanceHeader.topAnchor.constraint(equalTo: overviewLabel.bottomAnchor, constant: 20).isActive = true
        attendanceHeader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        attendanceHeader.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        attendanceHeader.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        attendanceLabel.topAnchor.constraint(equalTo: attendanceHeader.bottomAnchor, constant: 20).isActive = true
        attendanceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        attendanceLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        attendanceLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        mathHeader.topAnchor.constraint(equalTo: attendanceLabel.bottomAnchor, constant: 20).isActive = true
        mathHeader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        mathHeader.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        mathHeader.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        mathLabel.topAnchor.constraint(equalTo: mathHeader.bottomAnchor, constant: 20).isActive = true
        mathLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        mathLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        mathLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        readingHeader.topAnchor.constraint(equalTo: mathLabel.bottomAnchor, constant: 20).isActive = true
        readingHeader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        readingHeader.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        readingHeader.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        readingLabel.topAnchor.constraint (equalTo: readingHeader.bottomAnchor, constant: 20).isActive = true
        readingLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        readingLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        readingLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        writingHeader.topAnchor.constraint(equalTo: readingLabel.bottomAnchor, constant: 20).isActive = true
        writingHeader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        writingHeader.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        writingHeader.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
        
        writingLabel.topAnchor.constraint (equalTo: writingHeader.bottomAnchor, constant: 20).isActive = true
        writingLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        writingLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 5.0/6.0).isActive = true
        writingLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.01, constant: 8.0).isActive = true
    }
}

extension DetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? SchoolViewEntity else { return nil }
        
        var markerView: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: Constants.Identifiers.annotation) as? MKMarkerAnnotationView {
            
            dequeuedView.annotation = annotation
            markerView = dequeuedView
        } else {
            
            markerView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: Constants.Identifiers.annotation)
            markerView.canShowCallout = true
        }
        
        return markerView
    }
}

extension DetailViewController: UIScrollViewDelegate {
}
