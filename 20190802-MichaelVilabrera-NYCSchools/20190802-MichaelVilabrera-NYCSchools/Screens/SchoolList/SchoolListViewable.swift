//
//  SchoolListViewable.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import CoreLocation

protocol SchoolListViewable: BaseViewable {
    var schoolViewEntities: [SchoolViewEntity]? { get set }
    
    func reloadData()
}

