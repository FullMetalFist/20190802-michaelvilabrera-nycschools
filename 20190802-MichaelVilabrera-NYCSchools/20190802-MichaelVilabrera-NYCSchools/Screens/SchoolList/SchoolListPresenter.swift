//
//  SchoolListPresenter.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import Foundation

protocol SchoolListPresenterDelegate: class {
    var networkAdapter: NetworkAdapting { get set }
    var schoolListViewable: SchoolListViewable? { get set }
    var schoolMapper: SchoolMapping? { get set }
    
    func viewDidLoad()
    func fetchAll()
}

class SchoolListPresenter {
    
    private let schoolMapper: SchoolMapping = SchoolMapper()
    private let networkAdapter: NetworkAdapting
    
    private var schools = [School]()
    private var scores = [Score]()
    private var statusTuple = (school: false, score: false) {
        didSet {
            if self.statusTuple.school && self.statusTuple.score {
                self.schoolListViewable?.schoolViewEntities = self.schoolMapper.convertToViewEntity(self.schools, scores: self.scores)
                self.schoolListViewable?.reloadData()
            }
        }
    }
    
    var schoolListViewable: SchoolListViewable?
    
    init(networkAdapter: NetworkAdapting) {
        self.networkAdapter = networkAdapter
    }
    
    func viewDidLoad() {
        fetchAll()
    }
    
    private func fetchAll() {
        
        networkAdapter.fetchSchools { (schoolResult) in
            DispatchQueue.main.async {
                switch schoolResult {
                case let .success(_schools):
                    self.schools = _schools
                    self.statusTuple.school = true
                case let .failure(_error):
                    self.schoolListViewable?.showAlert(Constants.Alert.ok, message: _error.localizedDescription)
                }
            }
        }
        networkAdapter.fetchScores { (scoreResult) in
            DispatchQueue.main.async {
                switch scoreResult {
                case let .success(_scores):
                    self.scores = _scores
                    self.statusTuple.score = true
                case let .failure(_error):
                    self.schoolListViewable?.showAlert(Constants.Alert.ok, message: _error.localizedDescription)
                }
            }
        }
    }
}
