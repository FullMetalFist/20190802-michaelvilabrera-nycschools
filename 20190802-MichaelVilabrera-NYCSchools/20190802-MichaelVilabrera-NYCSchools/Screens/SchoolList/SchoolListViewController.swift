//
//  SchoolListViewController.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import UIKit

class SchoolListViewController: BaseViewController {
    
    private let tableView = UITableView()
    
    private var presenter: SchoolListPresenter?
    
    var viewable: SchoolListViewable?
    
    var schoolViewEntities: [SchoolViewEntity]? {
        didSet {
            reloadData()
        }
    }
    
    init(presenter: SchoolListPresenter) {
        super.init(nibName: nil, bundle: nil)
        self.presenter = presenter
        presenter.schoolListViewable = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Constants.Title.schoolList
        
        setupView()
        
        presenter?.viewDidLoad()
    }
    
    private func setupView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.Identifiers.tableViewCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 90.0
        view.addSubview(tableView)
        
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: margins.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
        ])
    }
}

extension SchoolListViewController: SchoolListViewable {
    
    func reloadData() {
        tableView.reloadData()
    }
}

extension SchoolListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewEntities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.tableViewCell) else { return UITableViewCell() }
        let baseView = UIView()
        baseView.backgroundColor = UIColor.cyan.withAlphaComponent(0.4)
        cell.selectedBackgroundView = baseView
        cell.textLabel?.text = schoolViewEntities?[indexPath.row].name
        cell.textLabel?.numberOfLines = 0
        return cell
    }
}

extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let entity = schoolViewEntities?[indexPath.row] else { return }
        
        let detailVC = DetailViewController(entity)
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
