//
//  ScoreViewEntity.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/5/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

struct ScoreViewEntity {
    
    let schoolName: String
    let attendance: String
    let reading: String
    let writing: String
    let math: String
}
