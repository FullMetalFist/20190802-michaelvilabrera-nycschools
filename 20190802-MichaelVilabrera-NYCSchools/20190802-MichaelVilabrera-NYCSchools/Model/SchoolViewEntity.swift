//
//  SchoolViewEntity.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import MapKit

class SchoolViewEntity: NSObject {
    let name: String
    let overview: String
    let address: String
    let score: ScoreViewEntity
    let coordinate: CLLocationCoordinate2D
    let hasTests: Bool
    
    init(name: String, overview: String, address: String, score: ScoreViewEntity, coordinate: CLLocationCoordinate2D, hasTests: Bool) {
        self.name = name
        self.overview = overview
        self.address = address
        self.score = score
        self.coordinate = coordinate
        self.hasTests = hasTests
        
        super.init()
    }
}

extension SchoolViewEntity: MKAnnotation {
    
    var title: String? {
        return name
    }
    
    var subtitle: String? {
        return address
    }
}
