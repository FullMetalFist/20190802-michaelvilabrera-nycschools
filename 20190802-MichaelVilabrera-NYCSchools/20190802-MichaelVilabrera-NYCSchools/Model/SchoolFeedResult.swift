//
//  SchoolFeedResult.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//


enum SchoolFeedResult {
    case success([School])
    case failure(APIError)
}
