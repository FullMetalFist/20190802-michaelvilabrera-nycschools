//
//  Score.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import Foundation

struct Score {
    
    let dbn: String
    let schoolName: String?
    let attendance: String?
    let reading: String?
    let writing: String?
    let math: String?
    
    init(dbn: String,
         schoolName: String? = nil,
         attendance: String? = nil,
         reading: String? = nil,
         writing: String? = nil,
         math: String? = nil) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.attendance = attendance
        self.reading = reading
        self.writing = writing
        self.math = math
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case attendance = "num_of_sat_test_takers"
        case reading = "sat_critical_reading_avg_score"
        case writing = "sat_writing_avg_score"
        case math = "sat_math_avg_score"
    }
}

extension Score: Decodable {
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decode(String.self, forKey: .dbn)
        schoolName = try values.decode(String.self, forKey: .schoolName)
        attendance = try values.decode(String.self, forKey: .attendance)
        reading = try values.decode(String.self, forKey: .reading)
        writing = try values.decode(String.self, forKey: .writing)
        math = try values.decode(String.self, forKey: .math)
    }
}
/*
 "dbn":"01M292",
 "school_name":"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
 "num_of_sat_test_takers":"29",
 "sat_critical_reading_avg_score":"355",
 "sat_math_avg_score":"404",
 "sat_writing_avg_score":"363"
 */
