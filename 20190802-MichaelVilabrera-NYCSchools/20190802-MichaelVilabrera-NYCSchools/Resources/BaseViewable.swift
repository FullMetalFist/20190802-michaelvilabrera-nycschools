//
//  BaseViewable.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

protocol BaseViewable: class {
    func showAlert(_ title: String, message: String)
    func viewDidLoad()
}
