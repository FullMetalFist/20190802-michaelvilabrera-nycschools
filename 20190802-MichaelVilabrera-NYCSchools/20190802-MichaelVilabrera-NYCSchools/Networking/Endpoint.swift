//
//  Endpoint.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/3/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import Foundation

protocol Endpoint {
    
    var base: String { get }
    var path: String { get }
}

extension Endpoint {
    
    var urlComponents: URLComponents? {
        var components = URLComponents(string: base)
        components?.path = path
        
        return components
    }
    
    var request: URLRequest? {
        let url = urlComponents?.url
        return URLRequest(url: url!)
    }
}

enum SchoolFeed {
    case school
    case score
}

extension SchoolFeed: Endpoint {
    
    var base: String {
        return "https://data.cityofnewyork.us"
    }
    
    var path: String {
        switch self {
        case .school: return "/resource/s3k6-pzi2.json"
        case .score: return "/resource/f9bf-2cp4.json"
        }
    }
}
