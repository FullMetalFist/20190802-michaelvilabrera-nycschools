//
//  NetworkAdapting.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import Foundation

protocol NetworkAdapting: class {
    var session: URLSession { get set }
    
    func fetchSchools(completion: @escaping (SchoolFeedResult) -> Void)
    func fetchScores(completion: @escaping (ScoreFeedResult) -> Void)
}
