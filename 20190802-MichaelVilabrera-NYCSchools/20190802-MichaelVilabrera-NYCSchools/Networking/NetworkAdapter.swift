//
//  NetworkAdapter.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import Foundation

enum APIError: Error {
    case requestFailed
    case conversionFailure
    case invalidData
    case responseFailure
    case parsingFailure
    
    var localizedDescription: String {
        switch self {
        case .requestFailed: return Constants.APIFailure.requestFailed
        case .conversionFailure: return Constants.APIFailure.conversionFailure
        case .invalidData: return Constants.APIFailure.invalidData
        case .responseFailure: return Constants.APIFailure.responseFailure
        case .parsingFailure: return Constants.APIFailure.parsingFailure
        }
    }
}

enum Result<T, APIError> {
    case success([T])
    case error(APIError)
}

class NetworkAdapter: NetworkAdapting {
    
    static let sharedInstance = NetworkAdapter()
    
    private(set) var schools = [School]()
    private(set) var scores = [Score]()

    lazy var session = URLSession.shared
    typealias completion = (Data?, URLResponse?, Error?) -> Void
    
    func fetchSchools(completion: @escaping (SchoolFeedResult) -> Void) {
        
        if !schools.isEmpty {
            completion(.success(schools))
        }
        
        guard let url = URL(string: "\(SchoolFeed.school.base)\(SchoolFeed.school.path)") else { return }
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(SchoolFeedResult.failure(.requestFailed))
                return
            }
            
            if httpResponse.statusCode == 200 {
                if let data = data {
                    do {
                        self.schools = try JSONDecoder().decode([School].self, from: data)
                        completion(SchoolFeedResult.success(self.schools))
                    } catch {
                        completion(SchoolFeedResult.failure(.conversionFailure))
                    }
                } else {
                    completion(SchoolFeedResult.failure(.invalidData))
                }
            } else {
                completion(SchoolFeedResult.failure(.responseFailure))
            }
        }
        dataTask.resume()
    }
    
    func fetchScores(completion: @escaping (ScoreFeedResult) -> Void) {
        
        if !scores.isEmpty {
            completion(.success(scores))
        }
        
        guard let url = URL(string: "\(SchoolFeed.school.base)\(SchoolFeed.score.path)") else { return }
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(ScoreFeedResult.failure(.requestFailed))
                return
            }
            
            if httpResponse.statusCode == 200 {
                if let data = data {
                    do {
                        self.scores = try JSONDecoder().decode([Score].self, from: data)
                        completion(ScoreFeedResult.success(self.scores))
                    } catch {
                        completion(ScoreFeedResult.failure(.conversionFailure))
                    }
                } else {
                    completion(ScoreFeedResult.failure(.invalidData))
                }
            } else {
                completion(ScoreFeedResult.failure(.responseFailure))
            }
        }
        dataTask.resume()
    }
}
