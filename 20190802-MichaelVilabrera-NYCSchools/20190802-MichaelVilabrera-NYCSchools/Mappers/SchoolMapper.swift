//
//  SchoolMapper.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/4/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import CoreLocation

protocol SchoolMapping {
    func convertToViewEntity(_ schools: [School], scores: [Score]) -> [SchoolViewEntity]
}

class SchoolMapper: SchoolMapping {
    
    func convertToViewEntity(_ schools: [School], scores: [Score]) -> [SchoolViewEntity] {
        
        var scoreDict = [String: ScoreViewEntity]()
        
        for score in scores {
            scoreDict[score.dbn] =
                ScoreViewEntity(
                    schoolName: score.schoolName ?? Constants.SchoolError.nameUnknown,
                    attendance: score.attendance ?? Constants.SchoolError.detailsUnknown,
                    reading: score.reading ?? Constants.SchoolError.noTest,
                    writing: score.writing ?? "",
                    math: score.math ?? ""
                )
        }
        
        return schools.compactMap({ (school) -> SchoolViewEntity in
            let latitude: Double!
            let longitude: Double!
            
            let appliedScore = scoreDict[school.dbn]
            if let lat = school.latitude, let lng = school.longitude {
                latitude = Double(lat) ?? 40.7128
                longitude = Double(lng) ?? -74.0060
            } else {
                latitude = 40.7128
                longitude = -74.0060
            }
            
            return SchoolViewEntity(name: school.name ?? Constants.SchoolError.nameUnknown,
                                    overview: school.overview ?? Constants.SchoolError.detailsUnknown,
                                    address: school.address ?? Constants.SchoolError.addressUnknown,
                                    score: appliedScore ?? ScoreViewEntity(
                                        schoolName: Constants.SchoolError.nameUnknown,
                                        attendance: Constants.SchoolError.noTest,
                                        reading: "",
                                        writing: "",
                                        math: ""),
                                    coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude),
                                    hasTests: appliedScore != nil
            )
        })
    }
}
