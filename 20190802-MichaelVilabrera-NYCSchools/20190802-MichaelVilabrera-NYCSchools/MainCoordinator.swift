//
//  MainCoordinator.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/2/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

import UIKit

class MainCoordinator {
    
    private let networkAdapter = NetworkAdapter.sharedInstance
    
    func start(_ window: UIWindow?) {
        
        let schoolListPresenter = SchoolListPresenter(networkAdapter: networkAdapter)
        let schoolListVC = SchoolListViewController(presenter: schoolListPresenter)
        schoolListVC.title = Constants.Title.schoolList
        let schoolListTabBarItem = UITabBarItem(title: Constants.Title.schoolList, image: UIImage(named: "classroom"), selectedImage: UIImage(named: "classroom"))
        schoolListVC.tabBarItem = schoolListTabBarItem
        let listNav = UINavigationController(rootViewController: schoolListVC)
        
        let mapPresenter = MapPresenter(networkAdapter: networkAdapter)
        let mapVC = MapViewController(presenter: mapPresenter)
        mapVC.title = Constants.Title.map
        let mapTabBarItem = UITabBarItem(title: Constants.Title.map, image: UIImage(named: "map"), selectedImage: UIImage(named: "map"))
        mapVC.tabBarItem = mapTabBarItem
        let mapNav = UINavigationController(rootViewController: mapVC)
        
        let tabBar = UITabBarController(nibName: nil, bundle: nil)
        tabBar.viewControllers = [listNav, mapNav]
        
        
        window?.rootViewController = tabBar
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
    }
}
