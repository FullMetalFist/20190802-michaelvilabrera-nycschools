//
//  Constants.swift
//  20190802-MichaelVilabrera-NYCSchools
//
//  Created by Michael Vilabrera on 8/2/19.
//  Copyright © 2019 Michael Vilabrera. All rights reserved.
//

struct Constants {
    
    struct Identifiers {
        static let tableViewCell = "tVCI"
        static let annotation = "aI"
        static let cluster = "Schools"
    }
        
    struct Title {
        static let schoolList = "School List"
        static let map = "Map"
    }
    
    struct SchoolError {
        static let nameUnknown = "Name Unknown"
        static let detailsUnknown = "Details Unknown"
        static let addressUnknown = "Address Unknown"
        static let noTest = "No SAT test takers at this location"
    }
    
    struct APIFailure {
        static let requestFailed = "Request Failed"
        static let conversionFailure = "ConversionFailure"
        static let invalidData = "Invalid Data"
        static let responseFailure = "Response Failure"
        static let parsingFailure = "Parsing Failure"
    }
    
    struct Alert {
        static let ok = "OK"
    }
    
    struct KeyPath {
        static let borderWidth = "borderWidth"
    }
    
    struct SATHeaders {
        static let attendance = "Attendance:"
        static let math = "Average Math Scores:"
        static let reading = "Average Reading Scores:"
        static let writing = "Average Writing Scores:"
    }
}
